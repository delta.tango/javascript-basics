import React,{Component} from 'react';
import axios from 'axios';

const Users = (props) => (
    <tbody>
        <td>{props.user.FirstName}</td>
        <td>{props.user.LastName}</td>
    </tbody>
);
export default class ViewUser extends Component{
    constructor(props){
        super(props);
        this.state={
            Users : []
        }
    }
    componentWillMount() {
        axios.get('http://localhost:4000/users')
            .then((response)=>{
                this.setState({Users : response.data})
            }).catch((err)=>{
                console.error(err);
        });
    }
    populateUsers(){
        return this.state.Users.map((currentUser, i)=>{
            return <Users user={currentUser} key={i} />;
        })
    }
    render() {
        return(
            <div>
                <table align="center" border="2px">
                    <thead>
                    <td>First Name</td>
                    <td>Last Name</td>
                    </thead>
                        {this.populateUsers()}
                </table>
            </div>
        );
    }
}