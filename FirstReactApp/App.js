import React from 'react';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import InsertUser from "./Components/comp.insertUser";
import ViewUser from "./Components/comp.viewUser";

function App() {
  return (
    <div className="App">
      <InsertUser/>
      <ViewUser/>
    </div>
  );
}

export default App;
