import React,{Component} from 'react';
import axios from 'axios';

export default class InsertUser extends Component{
    constructor(props){
        super(props);
        this.onChangeFirstName = this.onChangeFirstName.bind(this);
        this.onChangeLastName = this.onChangeLastName.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state={
          FirstName:"",
          LastName:""
        };
    }
    onChangeFirstName(e){
        this.setState({
            FirstName:e.target.value
        });
    }
    onChangeLastName(e){
        this.setState({
            LastName:e.target.value
        });
    }
    onSubmit(e){
        e.preventDefault();
        const newUser = {
            FirstName:this.state.FirstName,
            LastName:this.state.LastName
        };
        axios.post('http://localhost:4000/users',newUser).then((res)=>{
            console.log(res);
        });
        window.location.reload();//This will refresh the page
    }
    render() {
        return(
            <div>
                <form onSubmit={this.onSubmit} className="form-group" style={{marginTop:50}}>
                    <label>First Name: </label>
                    <input type="text" value={this.state.FirstName} onChange={this.onChangeFirstName}/>
                    <br/>
                    <label>Last Name: </label>
                    <input type="text" value={this.state.LastName} onChange={this.onChangeLastName}/>
                    <br/>
                    <input type="submit" className="btn btn-primary"/>
                </form>
            </div>
        );
    }
}