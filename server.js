const express = require ('express');        //import installed modules
const bodyParser = require('body-parser');

const app = express();      //creating app using express constructor
const PORT = 3000; //This will be the port which the app runs on

app.use(bodyParser.json()); //as our app should accept json request bodies

const users = [];           //for the moment we'll save data
                            // in this array

app.get('/users',(req,res)=>{
    res.status(200).json(users);    //This will send objects in user
});                                 //array along with status 200(OK)

app.post('/users',(req,res)=>{
    const requestObject = req.body;     //Assigns request to a variable
    users.push(requestObject);          //Adds object to array
    res.status(200).json(requestObject);//sends response with added object
});

app.listen(PORT,(err)=>{        //This publishes our app in localhost
   if(err) {                    //and listens for requests
       console.error(err);
   }else
       console.log('Server is up and running in Port :'+PORT);
});