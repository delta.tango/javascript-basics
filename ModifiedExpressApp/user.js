const mongoose = require('mongoose');

const User = new mongoose.Schema({      //we create a new directory as model to store our
    FirstName:{                         //mongoose models as a convention
        type:String
    },
    LastName:{
        type:String
    }
});

module.exports = mongoose.model('UserModel',User);