const express = require ('express');        //import installed modules
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const UserModel = require('./models/user');    //Schema Model


const app = express();      //creating app using express constructor
const PORT = 3000; //This will be the port which the app runs on

app.use(bodyParser.json()); //as our app should accept json request bodies

mongoose.connect('mongodb://localhost:27017/SampleAppDB',{useNewUrlParser:true});
const mongooseConnection = mongoose.connection;

mongooseConnection.once('open',()=>{
    console.log('mongoDB connection successful');
});

app.get('/users',(req,res)=>{
    UserModel.find((err,users)=>{
        if(err){
            console.error(err);
        }else
            res.status(200).json(users);//This will send documents of UserModel collection in Mongo DB
    })
});                                 //array along with status 200(OK)

app.post('/users',(req,res)=>{
    const requestObject = req.body;     //Assigns request to a variable
    const user = new UserModel(requestObject);

    user.save();    //This line will insert the JSON object to MongoDB

    res.status(200).json(requestObject);//sends response with added object
});

app.listen(PORT,(err)=>{        //This publishes our app in localhost
   if(err) {                    //and listens for requests
       console.error(err);
   }else
       console.log('Server is up and running in Port :'+PORT);
});